/**
 * @author Andres F. Garcia O.
 * @email andres.garcia@aliansap.com.co
 * @create date 2018-09-20 09:54:03
 * @modify date 2018-09-20 09:54:03
 * @desc Provider encargado de gestionar el proceso de login con passport y logout con passport.
*/

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { __API_URL, __ID_AUTH, __SECRET_AUTH, __GRANT_TYPE_AUTH } from './../../providers/constants';
import { Storage } from '@ionic/storage';

/*
Generated class for the AuthProvider provider.

See https://angular.io/guide/dependency-injection for more info on providers
and Angular DI.
*/
@Injectable()
export class AuthProvider {
  //
  // ────────────────────────────────────────────────────────── I ──────────
  //   :::::: A T R I B U T O S : :  :   :    :     :        :          :
  // ────────────────────────────────────────────────────────────────────
  //  
  private oauthUrl = __API_URL + "oauth/token";
  
  //
  // ────────────────────────────────────────────────────────────── II ──────────
  //   :::::: C O N S T R U C T O R : :  :   :    :     :        :          :
  // ────────────────────────────────────────────────────────────────────────
  //
  constructor(
    public http: HttpClient,
    private storage: Storage) {
    console.log('Hello AuthProvider Provider');
  }
  
  //
  // ────────────────────────────────────────────────────── III ──────────
  //   :::::: M E T O D O S : :  :   :    :     :        :          :
  // ────────────────────────────────────────────────────────────────
  //

  /**
   * @param  {string} user
   * @param  {string} password
   * 
   * * Este metodo se encarga de enviar a Passport los datos necesarios para la generación de un token.
   */
  getAccessToken(user: string, password: string) {
    let postData = {
      grant_type: __GRANT_TYPE_AUTH,
      client_id: __ID_AUTH,
      client_secret: __SECRET_AUTH,
      username: user,
      password: password,
      scope: "user-app"
    }
    
    return new Promise(resolve => {
      this.http.post(this.oauthUrl, postData).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  /**
   * * Se elimina el token de la session local
   */
  invalidateToken(){
    // TODO: Peticion para invalidar el server.
    // Delete token local
    this.storage.set('token', '');
    this.storage.set('token_refresh', '');
  }
}

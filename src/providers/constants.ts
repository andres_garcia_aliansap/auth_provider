/**
 * @author Andres F. Garcia O.
 * @email andres.garcia@aliansap.com.co
 * @create date 2018-09-20 10:10:05
 * @modify date 2018-09-20 10:10:05
 * @desc Clase para constantes dentro de la app.
*/

export const __API_URL = 'http://localhost:8000/';
export const __GRANT_TYPE_AUTH = 'password';
export const __SECRET_AUTH = 'AtbPLhOhiU703BNnzaflgwguBmpjfZSpCTUB7Je6';
export const __ID_AUTH = 1;
/**
 * @author Andres F. Garcia O.
 * @email andres.garcia@aliansap.com.co
 * @create date 2018-09-19 11:12:34
 * @modify date 2018-09-19 11:12:34
 * @desc [description]
*/

import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AuthProvider } from '../providers/auth/auth';
import { HttpModule} from '@angular/http';
import {HttpClientModule} from "@angular/common/http";
import { HttpClient } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { TestProvider } from '../providers/test/test';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpModule,
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    HttpClient,
    TestProvider,
    TestProvider
  ]
})
export class AppModule {}

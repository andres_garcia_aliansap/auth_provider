/**
 * @author Andres F. Garcia O.
 * @email andres.garcia@aliansap.com.co
 * @create date 2018-09-20 10:12:00
 * @modify date 2018-09-20 10:12:00
 * @desc Pagina Home
*/
import { Component, NgModule } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AuthProvider } from './../../providers/auth/auth';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
//
// ────────────────────────────────────────────────────────── I ──────────
//   :::::: A T R I B U T O S : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────
//



//
// ────────────────────────────────────────────────────────────── II ──────────
//   :::::: C O N S T R U C T O R : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────────
//
  constructor(
    public navCtrl: NavController, 
    public auth: AuthProvider, 
    private storage: Storage
    ) {
  }
  
//
// ────────────────────────────────────────────────────── III ──────────
//   :::::: M E T O D O S : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────
//

  /**
   * @param  {string} user
   * @param  {string} pass
   * 
   *  Metodo usado por la pantalla de login para realizar la solicitud de un token.
   */
  signIn(){
    // ! Reemplazar variables user y pass por las de su pantalla.
    this.auth.getAccessToken('superadmin@aliansap.com.co', 'alian$ap').then((data) => { 
      // Store token local
      this.storage.set('token', data['access_token']);
      this.storage.set('token_refresh', data['refresh_token']);
    });
  }
}
